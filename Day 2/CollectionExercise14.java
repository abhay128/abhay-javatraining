import java.util.Comparator;
import java.util.TreeMap;

public class CollectionExercise14 {
    public static void main(String[] args) {
        TreeMap<String, String> map = new TreeMap<>(new sort_key());

        map.put("K1","A");
        map.put("K3","B");
        map.put("K5","C");
        map.put("K2","D");
        map.put("K4","E");
        System.out.println(map);
    }
}
class sort_key implements Comparator<String>{
    @Override
    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }
}
