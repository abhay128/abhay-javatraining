import java.util.TreeMap;

public class CollectionExercise12 {
    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<>();

        map.put(1,"A");
        map.put(2,"B");
        map.put(3,"C");
        map.put(4,"D");
        map.put(5,"E");

        System.out.println("Original TreeMap : " + map);

        System.out.println("Keys greater than 3 in TreeMap : " + map.tailMap(3,false));
    }
}
