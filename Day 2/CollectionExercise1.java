import java.util.Arrays;

public class CollectionExercise1 {

    public static void main(String[] args) {
        int [] array = {1,2,3,4,5};
        int position = 1;
        int updateValue = 20;
        int updateValue2 = 40;
        int searchValue  = 4;

        System.out.println("Original Array  : "+ Arrays.toString(array));
        // By position

        array[position] = updateValue;

        System.out.println("Array after update by position : "+ Arrays.toString(array));

        // array update by value

        for (int i = 0; i< 5; i++){
            if(array[i] == searchValue){
                array[i] = updateValue2;
                
            }
        }
        System.out.println("Array after update by value : "+ Arrays.toString(array));
    }
}
