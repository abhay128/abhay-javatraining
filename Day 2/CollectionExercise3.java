import java.util.*;

public class CollectionExercise3 {
    public static void main(String[] args) {
        ArrayList list1 = new ArrayList<String>();
        list1.add("A");
        list1.add("B");
        list1.add("C");
        list1.add("D");

        // copying elements of one arraylist to another by reference
        ArrayList list2 = list1;
        System.out.println("Elements of List 1 : "+ list1);
        System.out.println("Elements of List 2 : "+ list2);

        // copying elements one by one
        ArrayList list3 = new ArrayList<String>();
        for(int i = 0; i< list1.size(); i++){
            list3.add(list1.get(i));
        }
        System.out.println("Elements of List 3 : "+ list3);
    }
}
