import java.util.TreeSet;

public class CollectionExercise9 {
    public static void main(String[] args) {
        TreeSet<Integer> set1 = new TreeSet<>();

        set1.add(10);
        set1.add(20);
        set1.add(30);
        set1.add(40);
        set1.add(50);
        set1.add(60);
        set1.add(70);

        int givenElement = 39;

        System.out.println("Strictly less than " + givenElement + " : " + set1.lower(givenElement));
    }
}
