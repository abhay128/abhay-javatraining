class Node{
    int data;
    Node nextNode;
}
class LinkedList {
    private Node head;

    public void insert(int data){
        Node node = new Node();
        node.data = data;
        node.nextNode = null;
        // check if list is empty
        if(this.head == null){
            this.head = node;
            return;
        }
        Node tempNode = this.head;
        while (tempNode.nextNode != null){
            tempNode = tempNode.nextNode;
        }
        tempNode.nextNode = node;
    }

    public void insertNthPosition(int data, int position){
        Node node = new Node();
        node.data = data;
        node.nextNode = null;

        if(this.head == null){
            if(position != 0){
                return;
            }else{
                this.head = node;
            }
        }
        if(this.head != null && position == 0){
            node.nextNode = this.head;
            this.head = node;
            return;
        }

        Node current = this.head;
        Node previous = null;

        int index = 0;

        while(index < position){
            previous = current;
            current = current.nextNode;

            if(current == null){
                break;
            }
            index++;
        }
        node.nextNode = current;
        previous.nextNode = node;
    }

    public void printList(){
        if(this.head == null){
            return;
        }
        Node tempNode = this.head;
        while (tempNode != null){
            System.out.print(tempNode.data + " -> ");
            tempNode = tempNode.nextNode;
        }
        System.out.println("null");
    }
}
public class CollectionExercise5 {
    public static void main(String[] args) {
        LinkedList list1 = new LinkedList();
        list1.insert(10);
        list1.insert(20);
        list1.insert(30);
        list1.insert(40);
        list1.insert(50);
        System.out.println("Original LinkedList :");
        list1.printList();
        System.out.println("After update :");
        list1.insertNthPosition(35,3);
        list1.printList();
    }
}
