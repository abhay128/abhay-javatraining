import java.util.TreeSet;

public class CollectionExercise8 {
    public static void main(String[] args) {
        TreeSet<String> set1 = new TreeSet<>();
        set1.add("A");
        set1.add("B");
        set1.add("C");
        set1.add("D");
        set1.add("E");

        System.out.println("First treeset : " + set1);

        TreeSet<String> set2 = new TreeSet<>();
        set2.add("A");
        set2.add("B");
        set2.add("C");
        set2.add("D");
        set2.add("E");

        System.out.println("Second treeset : " + set2);

        boolean check = set1.equals(set2);

        System.out.println("Are both sets equal : " + check);
    }
}
