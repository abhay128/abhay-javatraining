import java.util.*;

public class CollectionExercise7 {
    public static void main(String[] args) {
        Set<String> setObj = new HashSet<>();
        setObj.add("B");
        setObj.add("H");
        setObj.add("A");
        setObj.add("G");
        setObj.add("A");
        setObj.add("T");

        System.out.println("Hashset : " + setObj);

        Set<String> treesetObj = new TreeSet<>();
        treesetObj.addAll(setObj);

        System.out.println("Treeset: " + treesetObj);
    }
}
