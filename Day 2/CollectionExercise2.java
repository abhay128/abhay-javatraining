import java.util.*;

public class CollectionExercise2 {

    public static void main(String[] args) {
        List list1 = new ArrayList<String>();
        list1.add("A");
        list1.add("B");
        list1.add("C");
        list1.add("D");

        int index = list1.indexOf("B");
        if(index == -1)
            System.out.println("Element B is not present in the list");
        else
            System.out.println("Element B is present at index " + index);

    }
}
