import java.util.TreeMap;

public class CollectionExercise13 {
    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<>();

        map.put(1,"A");
        map.put(2,"B");
        map.put(3,"C");
        map.put(4,"D");
        map.put(5,"E");

        System.out.println("Original Treemap : " + map);
        System.out.println("Reverse order view of keys in treemap : " + map.descendingKeySet());

    }
}
