import java.util.*;

public class CollectionExercise4 {
    public static void main(String[] args) {
        LinkedList<String> list1 = new LinkedList<>();
        list1.add("A");
        list1.add("B");
        list1.add("C");
        list1.add("D");

        System.out.println("Original List : " + list1);
        Iterator<String> iterator = list1.descendingIterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
