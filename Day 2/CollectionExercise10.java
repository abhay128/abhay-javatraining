import java.util.PriorityQueue;

public class CollectionExercise10 {
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();

        queue.add(10);
        queue.add(20);
        queue.add(40);
        queue.add(30);
        queue.add(12);

        System.out.println("The priority queue :" + queue);

        Object[] arr = queue.toArray();

        System.out.println("The Array is : ");
        for (int i = 0; i < arr.length; i++ ){
            System.out.println(arr[i]);
        }
    }
}
