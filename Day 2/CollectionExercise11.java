import java.util.HashMap;

public class CollectionExercise11 {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        map.put(1,"A");
        map.put(2,"B");
        map.put(3,"C");
        map.put(4,"D");
        map.put(5,"E");

        System.out.println("Initial mappings are : " + map);
        System.out.println("Check if key '3' is present ?   : " + map.containsKey(3));
        System.out.println("Check if key '7' is present ?   : " + map.containsKey(7));
        System.out.println("Check if value 'D' is present ? : " + map.containsValue("D"));
        System.out.println("Check if value 'Z' is present ? : " + map.containsValue("Z"));
    }
}
