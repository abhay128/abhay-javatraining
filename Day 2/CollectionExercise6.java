import java.util.*;
public class CollectionExercise6 {
    public static void main(String[] args) {
        ArrayList list1 = new ArrayList<String>();
        list1.add("A");
        list1.add("B");
        list1.add("C");
        list1.add("D");

        System.out.println("Original linkedlist : "+ list1);
        Collections.shuffle(list1);

        System.out.println(" After first shuffle : "+ list1);

        Collections.shuffle(list1);
        System.out.println(" After second shuffle : "+ list1);
    }
}
