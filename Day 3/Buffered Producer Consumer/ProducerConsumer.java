import java.util.LinkedList;

public class ProducerConsumer {
    LinkedList<Integer> list = new LinkedList<>();
    int volume = 5;

    public void producer() throws InterruptedException
    {
        int value = 0;
        while (true) {
            synchronized (this)
            {
                while (list.size() == volume)
                    wait();
                System.out.println("Start enqueing. Value produced:" +value);
                list.add(value++);
                System.out.println("Successful enqueing");
                notify();
                Thread.sleep(1000);
            }
        }
    }

    public void consumer() throws InterruptedException
    {
        while (true) {
            synchronized (this)
            {
                while (list.size() == 0)
                    wait();
                int val = list.removeFirst();
                System.out.println("Successful consumption. Value consumed:" +val);
                notify();


                Thread.sleep(1000);
            }
        }
    }
}
