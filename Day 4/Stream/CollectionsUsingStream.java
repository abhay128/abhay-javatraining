import java.util.Arrays;

import java.util.stream.Stream;

public class CollectionsUsingStream {
    public static void main(String[] args) {

        String[] list = {"A","B","C","D","E"};

        //replace element in an array
        Stream<String> matchedResult = Arrays.stream(list).map(x-> x.replace("A", "Z"));
        System.out.println("String list after replacement and stored in new string list");
        matchedResult.forEach(i-> System.out.println(i));

        System.out.print("Return string list matching 'A' :  ");
        //Find element in the list
        String result = Arrays.stream(list)
                .filter( x -> x.equals("A"))
                .findAny().orElse(null);
        System.out.println(result);

        //Stream.match()
        System.out.print("Check if string list has 'B' : ");
        boolean matched = Arrays.stream(list)
                .map(x -> x.toUpperCase())
                .anyMatch(x -> x.equals("B"));
        System.out.println(matched);

        //copy one array into another
        System.out.println("Copy array");
        String[] copiedArray = Arrays.stream(list).toArray(String[]::new);

        Arrays.stream(copiedArray).forEach(i-> System.out.println(i));



    }
}
