
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample01 {
    public static void main(String[] args) {
        String [] arr = {"a","b","c","d"};
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");

        Stream<String> arrayStream = Stream.of(arr);
        //below one creates the list object stream
        Stream<List<String>> listStream = Stream.of(list);
        // to create the individual elements of list to stream
        Stream<String> listStream1 = list.stream();

        Stream<String> stream = Stream.of("a","b","c","d");

//        Stream.of(arrayStream,listStream,stream);
        //System.out.println(Stream.of(arrayStream,listStream,stream));

        //Stream.concat(Stream.concat(arrayStream,listStream),stream).forEach(i -> System.out.println(i));

        Stream<String> allStream = Stream.concat(Stream.concat(arrayStream,listStream1),stream);

        allStream
                .filter(s -> s.charAt(0) <= 90)
                .forEach(i -> System.out.println(i));


    }
}
